# Chart Task
## Description

This project displays the `Attention Score` of two participants during an event. It displays the line without any gaps. Moreover, the values are populated after every 10 seconds.
## Steps to Run the Project

### Pre-Conditions:
- Internet should be connected because the chart library is included using CDN

### Steps:
1. Download the `Chart Task` Project
2. Open the `index.html` file in the browser

